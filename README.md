### Cara menjalankan program
=============================

**Spesifikasi minimal software untuk menjalankan program**
* php-cli minimal versi **7.2**.
* terminal / command prompt.
* browser (google chrome).

=============================
**Cara menjalankan program**
1. Buka terminal.
2. Masuk ke directory.
3. Jalankan web server php, ketik `php -S localhost:8000`.
4. Buka browser ketikan alamat `http://localhost:8000/`.
5. Masukkan kalimat anda, dan pilih proses encode / decode.
