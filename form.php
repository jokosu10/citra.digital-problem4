<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
        <meta charset="utf-8">
        <style type="text/css">
            .box {
                margin: 250px auto;
                width: 400px;
                padding: 10px;
            }
            input[type=text], input[type=password] {
                margin: 5px auto;
                width: 100%;
                padding: 10px;
            }
            input[type=submit] {
                margin 5px auto;
                float: right;
                padding: 5px;
                width: 90px;
                border: 1px solid #fff;
                color: #fff;
                background: red;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="box">
            <form method="post" action="#">
                <input type="text" name="string" placeholder="Masukan kalimat Anda"><br>
                <button type='button' id="dekode" name="dekode" class='btn btn-primary center-block'>Dekode</button>
                <button type='button' id="encode" name="encode" class='btn btn-primary center-block'>Enkode</button>
                <span name="result">Hasilnya</span>
            </form>
        </div>
    </body>
     <script>
    $('#encode').click(function()
    {
        alert('encode');
        $.ajax(
        {
            url: "encode.php",
            type: "POST",
            dataType: "json",
            data: {$result},

        });
    });

    $('#dekode').click(function()
    {
        alert('dekode');
        $.ajax(
        {
            url: "decode.php",
            type: "POST",
            dataType: "json",
            data: {$result},

        });
    });
    </script>
</html>
