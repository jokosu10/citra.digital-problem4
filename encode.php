<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-Type: text/html; charset=utf-8');
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    function encodeProcess($string)
    {
        $hurufHidup = ['a','i','u','e','o'];
        $arrayString = str_split($string);
        foreach(array_values($arrayString) as $i => $value) {
            if (in_array($value, $hurufHidup)) {
                echo strtoupper($value);
            } else {
                echo $i."+";
            }
        }
    }
    // get posted data
    $data = json_decode(file_get_contents("php://input"));

    if (!empty($data->kalimat)) {
        $teksB = 'b';
        $teksP = 'p';
        $posisiB = strpos($data->kalimat, $teksB);
        $posisiP = strpos($data->kalimat, $teksP);

        // cek bila ada huruf p & b
        if ($posisiP === false && $posisiB === false || $posisiB === false && $posisiP === false || $posisiP !== false && $posisiB === false || $posisiB !== false && $posisiP === false)  {
            $data->kalimat = $data->kalimat;
        } else {
            // $tmp = $posisiB;
            // $posisiB = $posisiP;
            // $posisiB = 'p';
            // $posisiP = $tmp;
            // $posisiP = 'b';

            // $data->kalimat = $posisiB."".$data->kalimat."".$posisiP;

            $data->kalimat = substr_replace($data->kalimat, $teksB, $posisiP, 1);
            $data->kalimat = substr_replace($data->kalimat, $teksP, $posisiB, 1);

        }

       $result = encodeProcess($data->kalimat);
        // set response code - 201 created
        http_response_code(201);

        // tell the user
        echo json_encode(array("message" => "Encode suceess.", "data"=>$result));
    } else {
        // set response code - 400 bad request
        http_response_code(400);

        // tell the user
        echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
    }


?>
