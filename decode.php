<?php

    header("Access-Control-Allow-Origin: *");
    header('Content-Type: text/html; charset=utf-8');
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    function decodeProcess($string)
    {
        $hurufHidup = ['A','I','U','E','O'];
        $arrayString = str_split($string);
        foreach(array_values($arrayString) as $i => $value) {
            if (in_array($value, $hurufHidup)) {
                echo strtolower($value);
            } else {
                echo $value;
            }
        }
        echo "\n";
    }

   // get posted data
    $data = json_decode(file_get_contents("php://input"));

    if (!empty($data->kalimat)) {
        $teksB = 'b';
        $teksP = 'p';
        $posisiB = strpos($data->kalimat, $teksB);
        $posisiP = strpos($data->kalimat, $teksP);

        // cek bila ada huruf p & b
        if ($posisiP === false && $posisiB === false || $posisiB === false && $posisiP === false || $posisiP !== false && $posisiB === false || $posisiB !== false && $posisiP === false)  {
            $data->kalimat = $data->kalimat;
        } else {
             $tmp = $posisiB;
            $posisiB = $posisiP;
            $posisiB = 'p';
            $posisiP = $tmp;
            $posisiP = 'b';

            $data->kalimat = $posisiB."".$data->kalimat."".$posisiP;

        }

        $result = json_encode(decodeProcess($data->kalimat));
        $jsonResult = json_encode($result);
        // set response code - 201 created
        http_response_code(201);

        // tell the user
        echo json_decode(array("message" => "Encode suceess.", "data"=>$result));
    } else {
        // set response code - 400 bad request
        http_response_code(400);

        // tell the user
        echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
    }

?>
